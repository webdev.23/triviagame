function fetchCategories() {
    fetch("http://localhost:2000/getQuestion")
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            console.log("API response data:", data);
            if (data.res && Array.isArray(data.res)) {
                const uniqueCategories = [...new Set(data.res.map(item => item.category))];
                const categoriesContainer = document.getElementById('categories');
                categoriesContainer.innerHTML = '';
                uniqueCategories.forEach(category => {
                    const categoryElement = document.createElement('h3');
                    categoryElement.textContent = category;
                    categoryElement.addEventListener('click', () => {
                        window.location.href = `questions.html?category=${encodeURIComponent(category)}`;
                    });
                    categoriesContainer.appendChild(categoryElement);
                });
            } else {
                throw new Error("Unexpected data format");
            }
        })
        .catch(error => {
            alert("API request failed.");
            console.error('Error fetching categories:', error);
        });
}

fetchCategories();
