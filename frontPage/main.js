document.getElementById('startGameButton').addEventListener('click', function() {
    const player1Name = document.getElementById('player1').value.trim();
    const player2Name = document.getElementById('player2').value.trim();
    if (player1Name === '' || player2Name === '') {
        alert('Please enter names for both players.');
    } else {
        window.location.href = 'category.html';
    }
});
