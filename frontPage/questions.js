const urlParams = new URLSearchParams(window.location.search);
const category = urlParams.get('category');
let questions = [];
let currentQuestionIndex = 0;
let player1Score = 0;
let player2Score = 0;

function fetchQuestions() {
    fetch("http://localhost:2000/getQuestion")
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            questions = Array.isArray(data.res) ? data.res : [data.res];
            questions = questions.filter(q => q.category === category); // Filter by selected category
            distributeQuestionsByDifficulty();
        })
        .catch(error => {
            console.error("Error fetching questions:", error);
            document.getElementById("question").innerHTML = `<h2>Failed to fetch questions. Please try again later.</h2>`;
        });
}

function distributeQuestionsByDifficulty() {
    const hardQuestions = questions.filter(q => q.difficulty === 'hard');
    const mediumQuestions = questions.filter(q => q.difficulty === 'medium');
    const easyQuestions = questions.filter(q => q.difficulty === 'easy');
    const distributedQuestions = [...hardQuestions, ...mediumQuestions, ...easyQuestions];
    displayQuestion(distributedQuestions);
}

function displayQuestion(questions) {
    if (currentQuestionIndex < questions.length) {
        const questionData = questions[currentQuestionIndex];
        document.getElementById("question").innerHTML = `<h2>${questionData.question}</h2>`;
        document.getElementById("answerForm").reset();
        document.getElementById("result").innerHTML = '';
        document.getElementById("nextQuestion").style.display = 'none';
        document.getElementById("currentLevel").innerText = `Current Level: ${questionData.difficulty}`;
    } else {
        showGameOver();
    }
}

function normalizeAnswer(answer) {
    return answer.trim().toLowerCase();
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, (txt) => {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

document.getElementById("answerForm").addEventListener("submit", function(event) {
    event.preventDefault();
    const player1Answer = normalizeAnswer(document.getElementById("player1Answer").value);
    const player2Answer = normalizeAnswer(document.getElementById("player2Answer").value);
    const player1TitleCasedAnswer = toTitleCase(player1Answer);
    const player2TitleCasedAnswer = toTitleCase(player2Answer);
    const correctAnswer = toTitleCase(normalizeAnswer(questions[currentQuestionIndex].correctAnswer));

    let player1Result = player1TitleCasedAnswer === correctAnswer ? "Correct" : "Incorrect";
    let player2Result = player2TitleCasedAnswer === correctAnswer ? "Correct" : "Incorrect";

    if (player1Result === "Correct") {
        console.log(`Player 1 answered correctly. Difficulty: ${questions[currentQuestionIndex].difficulty}`);
        switch (questions[currentQuestionIndex].difficulty) {
            case "hard":
                player1Score += 20;
                break;
            case "medium":
                player1Score += 15;
                break;
            case "easy":
                player1Score += 10;
                break;
        }
    }
    if (player2Result === "Correct") {
        console.log(`Player 2 answered correctly. Difficulty: ${questions[currentQuestionIndex].difficulty}`);
        switch (questions[currentQuestionIndex].difficulty) {
            case "hard":
                player2Score += 20;
                break;
            case "medium":
                player2Score += 15;
                break;
            case "easy":
                player2Score += 10;
                break;
        }
    }

    document.getElementById("player1Score").innerText = player1Score;
    document.getElementById("player2Score").innerText = player2Score;

    let resultHtml = `
        <p>Player 1 Answer: ${player1TitleCasedAnswer} - ${player1Result}</p>
        <p>Player 2 Answer: ${player2TitleCasedAnswer} - ${player2Result}</p>
    `;
    document.getElementById("result").innerHTML = resultHtml;
    document.getElementById("nextQuestion").style.display = 'block';
});

document.getElementById("nextQuestion").addEventListener("click", function() {
    currentQuestionIndex++;
    if (currentQuestionIndex < questions.length) {
        displayQuestion(questions);
    } else {
        showGameOver();
    }
});

function showGameOver() {
    document.getElementById("question").innerHTML = `<h2>No more questions available. Game Over!</h2>`;
    document.getElementById("answerForm").style.display = 'none';
    document.getElementById("nextQuestion").style.display = 'none';
    document.getElementById("currentLevel").innerText = '';
    let winner = "";
    if (player1Score > player2Score) {
        winner = "Player 1";
    } else if (player2Score > player1Score) {
        winner = "Player 2";
    } else {
        winner = "It's a tie!";
    }
    let resultHtml = 
    `
        <h2>Winner: ${winner}</h2>
        <p>Player 1 Final Score: ${player1Score}</p>
        <p>Player 2 Final Score: ${player2Score}</p>
    ` ;
    document.getElementById("result").innerHTML = resultHtml;
}

fetchQuestions();
