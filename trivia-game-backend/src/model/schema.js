const mongoose = require('mongoose');
const quizQuestionSchema = new mongoose.Schema({
    category: String,
    correctAnswer: String,
    incorrectAnswers: [String],
    question:String,
    tags: [String],
    type: String,
    difficulty: String,
    regions: [String],
    isNiche: Boolean
});
const QuizQuestion = mongoose.model('QuizQuestion', quizQuestionSchema);
module.exports = QuizQuestion;
