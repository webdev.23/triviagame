const QuizQuestion = require('../model/schema');
const saveData = async (req, res) => {
    try {
        const { category, correctAnswer, incorrectAnswers, question, tags, type, difficulty, regions, isNiche } = req.body;
        const quizQuestion = new QuizQuestion({
            category,
            correctAnswer,
            incorrectAnswers, 
            question, 
            tags,
            type,
            difficulty,
            regions,
            isNiche
        });

        const data = await quizQuestion.save();
        console.log(data, "Data saved successfully");

        res.status(200).json({ message: 'Quiz question saved successfully', data });
    } catch (error) {
        console.error('Error saving quiz question to MongoDB:', error);
        res.status(500).json({ error: 'An error occurred while saving quiz question' });
    }
};

module.exports = { saveData };
