const QuizQuestion = require('../model/schema');

const DATA = async (req, res) => {
    const { category } = req.query; 

    try {
        const wholeData = await QuizQuestion.findOne({ category });
        if (wholeData) {
            res.status(200).json({ "res": wholeData });
            console.log(wholeData);
        } else {
            res.status(404).json({ "res": "There is no data" }); 
            console.log("There is no data");
        }
    } catch (error) {
        res.status(500).json({ "error": "Something went wrong", error });
        console.log(error);
    }
}
module.exports = { DATA };
