const QuizQuestion = require('../model/schema');
const getData=async(req,res)=>{
    try {
    const wholeData= await QuizQuestion.find()
    if (wholeData.length>0){
        res.status(200).json({"res":wholeData})
        console.log(wholeData)
    }
    else{
        res.status(201).json({"res":"There is no data"})
        console.log("There is no data")
    }
    } catch (error) {
        res.status(500).json({"error":"Something went wrong",error})
        console.log(error)
    }
}
module.exports={getData}