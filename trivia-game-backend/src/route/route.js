const express = require('express');
const router = express.Router();
const info = require('../controller/post');
const { getData } = require('../controller/api');
router.post('/addQuestion', info.saveData);
router.get('/getQuestion', getData);
module.exports = router;
