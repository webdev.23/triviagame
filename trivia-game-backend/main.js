const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors'); 
const routes = require("./src/route/route");

const app = express();
app.use(bodyParser.json());
app.use(cors()); 
mongoose.connect('mongodb://127.0.0.1:27017/QuizApp', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Connected to MongoDB');
}).catch((error) => {
    console.error('Error connecting to MongoDB:', error);
});

app.use("/", routes);
app.listen(2000, () => {
    console.log(`Server is running on port 2000`);
});
